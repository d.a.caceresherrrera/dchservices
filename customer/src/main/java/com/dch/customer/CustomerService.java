package com.dch.customer;

import com.dch.amqp.RabbitMQMessageProducer;
import com.dch.clients.fraud.FraudCheckResponse;
import com.dch.clients.fraud.FraudClient;

import com.dch.clients.notification.NotificationRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@AllArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final RestTemplate restTemplate;

    private final FraudClient fraudClient;
    private final RabbitMQMessageProducer rabbitMQMessageProducer;
    public void registerCustomer(CustomerRegistrationRequest request) throws IllegalAccessException {
        Customer customer = Customer.builder()
                .firstName(request.firstName())
                .lastName(request.lastName())
                .email(request.email())
                .build();
        customerRepository.saveAndFlush(customer);
        FraudCheckResponse fraudCheckResponse = fraudClient.isFraudster(customer.getId());
        if (fraudCheckResponse.isFraudster()) {
            throw new IllegalAccessException("fraudster");
        }
        /*
        notificationClient.sendNotification(new NotificationRequest(
                customer.getId(), customer.getEmail(), String.format("Welcome to dch services %s", customer.getFirstName())
        ));*/
        NotificationRequest notificationRequest = new NotificationRequest(
                customer.getId(),
                customer.getEmail(),
                String.format("Welcome to dch services %s",
                        customer.getFirstName())
        );
        rabbitMQMessageProducer.publish(
                notificationRequest,
                "internal.exchanges",
                "internal.notification.routing-key"
        );

    }
}
