package com.dch.clients.fraud;

public record FraudCheckResponse(Boolean isFraudster) {
}
